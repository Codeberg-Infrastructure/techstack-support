# Tech-Stack Support

Sometimes, we are looking for expertise with specific parts in our stack where we appreciate external input.

If you want to help, please check the open issues and see if you can support us with your experience,
or join dedicated teams.


## How it works

The workflow consists of two parts.
We really appreciate your external input,
so in any case, let us know if we can reach out to you.

### Issue-Based

If we seek input on a certain topic,
we open an issue and label it with the technology it requires knowledge in (e.g. "HAProxy" or "Ceph").
Feel free to filter for issues you can comment in and share your thoughts.

### Team-Based

If you have knowledge in a specific domain,
feel free to open an issue and let us know.
In case we need assistance,
we can reach out to you.

We will consider to create teams in Codeberg-Infrastructure/ to ping you.

---

Thanks a lot.

Your Codeberg Team.

Please note: This is an experiment to deal with the problems described in [this blog post](https://blog.codeberg.org/the-hardest-scaling-issue.html).

**PS: We are also looking for people to manage discussions and ensure the results are summarized and communicated back.**
If you are interested to help us by taking such a role (for how long you wish), please let us know via an issue.